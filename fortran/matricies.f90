Program matrix
	
	USE IFPORT

	parameter (n = 500)
	integer(4) i,j,k,l,m,iseed

	double precision A(n,n), B(n,n), C(n,n), tm(3)

	!$ integer omp_get_num_threads
	!$ double precision omp_get_wtime
	!$ double precision tm0,tm1
	!$omp parallel
	!$ write(*,*) 'OMP parallel with ', omp_get_num_threads()
	!$omp end parallel

	iseed = 100
	call SEED(1995)
	do i = 1,n
		do j = 1,n
			A(i,j) = RAN(iseed)*100
			B(i,j) = RAN(iseed)*100
			C(i,j) = 0
		end do
	end do

	!$ tm0 = omp_get_wtime()

	write(*,*) 'Calculations...'

	!$omp parallel
	!$omp do private (i,j,k)
	do j = 1,n
		do k = 1,n
			do i = 1,n
				C(i,j) = C(i,j) + A(i,k)*B(k,j)
			end do
		end do
	end do

	!$omp end do
	!$omp end parallel

	!$ tm1=omp_get_wtime()
	!$ write(*,*) tm1-tm0

End